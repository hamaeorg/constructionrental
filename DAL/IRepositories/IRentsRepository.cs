﻿using DAL.IEF;
using Domain;
using Domain.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.IRepositories
{
    public interface IRentsRepository : IRepository<Rent>
    {
        List<Rent> FindByStatus(Status status);
    }
}

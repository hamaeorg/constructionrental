﻿using DAL.IEF;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.IRepositories
{
    public interface IEquipmentTypePriceRepository : IRepository<EquipmentTypePrice>
    {
        List<EquipmentTypePrice> FindByEquipmentTypeId(long id);
    }
}

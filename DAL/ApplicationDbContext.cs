﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using DAL.Helpers;

namespace DAL
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Equipment> Equipment { get; set; }
        public DbSet<EquipmentType> EquipmentTypes { get; set; }
        public DbSet<EquipmentTypePrice> EquipmentTypePrices { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<Rent> Rents { get; set; }
        public DbSet<RentItem> RentItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<EquipmentType>().HasData(ObjectSeedData.GetEquipmentTypes());
            modelBuilder.Entity<Price>().HasData(ObjectSeedData.GetPrices());
            modelBuilder.Entity<EquipmentTypePrice>().HasData(ObjectSeedData.GetEquipmentTypePrices());
            modelBuilder.Entity<Equipment>().HasData(ObjectSeedData.GetEquipment());
            modelBuilder.Entity<RentItem>().HasData(ObjectSeedData.GetRentItems());
            modelBuilder.Entity<Rent>().HasData(ObjectSeedData.GetRents());
        }
    }
}

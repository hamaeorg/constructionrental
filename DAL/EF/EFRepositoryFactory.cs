﻿using DAL.EF;
using DAL.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class EFRepositoryFactory : IRepositoryFactory
    {
        private readonly Dictionary<Type, Func<ApplicationDbContext, object>> _customRepositoryFactories
            = GetCustomRepoFactories();

        private static Dictionary<Type, Func<ApplicationDbContext, object>> GetCustomRepoFactories()
        {
            return new Dictionary<Type, Func<ApplicationDbContext, object>>()
            {
                {typeof(IEquipmentRepository), (dataContext) => new EFEquipmentRepository(dataContext as ApplicationDbContext) },
                {typeof(IEquipmentTypePriceRepository), (dataContext) => new EFEquipmentTypePriceRepository(dataContext as ApplicationDbContext) },
                {typeof(IEquipmentTypeRepository), (dataContext) => new EFEquipmentTypeRepository(dataContext as ApplicationDbContext) },
                {typeof(IPriceRepository), (dataContext) => new EFPriceRepository(dataContext as ApplicationDbContext) },
                {typeof(IRentItemsRepository), (dataContext) => new EFRentItemsRepository(dataContext as ApplicationDbContext) },
                {typeof(IRentsRepository), (dataContext) => new EFRentsRepository(dataContext as ApplicationDbContext) },
            };
        }

        public Func<ApplicationDbContext, object> GetCustomRepositoryFactory<TRepoInterface>() where TRepoInterface : class
        {
            _customRepositoryFactories.TryGetValue(
                typeof(TRepoInterface),
                out Func<ApplicationDbContext, object> factory
            );
            return factory;
        }

        public Func<ApplicationDbContext, object> GetStandardRepositoryFactory<TEntity>() where TEntity : class
        {

            return (dataContext) => new EFRepository<TEntity>(dataContext as ApplicationDbContext);
        }
    }
}

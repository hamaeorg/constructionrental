﻿using Domain;
using Domain.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Helpers
{
    public static class ObjectSeedData
    {
        public static List<EquipmentType> GetEquipmentTypes()
        {
            return new List<EquipmentType>{
                new EquipmentType { EquipmentTypeId = 1, Type = EqType.Heavy, LoyaltyPoints = 2},
                new EquipmentType { EquipmentTypeId = 2, Type = EqType.Regular, LoyaltyPoints = 1},
                new EquipmentType { EquipmentTypeId = 3, Type = EqType.Specialized, LoyaltyPoints = 1}
            };
        }
        public static List<Price> GetPrices()
        {
            return new List<Price>{
                new Price { PriceId = 1, Type = PriceType.OneTime, PriceValue = new decimal(100), IsRecurrent = false },
                new Price { PriceId = 2, Type = PriceType.PremiumDaily, PriceValue = new decimal(60), IsRecurrent = true },
                new Price { PriceId = 3, Type = PriceType.RegularDaily, PriceValue = new decimal(40), IsRecurrent = true }
            };
        }
        public static List<EquipmentTypePrice> GetEquipmentTypePrices()
        {
            return new List<EquipmentTypePrice>{
                new EquipmentTypePrice { EquipmentTypePriceId = 1, EquipmentTypeId = 1, PriceId = 1, Duration = 0 },
                new EquipmentTypePrice { EquipmentTypePriceId = 2, EquipmentTypeId = 1, PriceId = 2, Duration = 0 },
                new EquipmentTypePrice { EquipmentTypePriceId = 3, EquipmentTypeId = 2, PriceId = 1, Duration = 0 },
                new EquipmentTypePrice { EquipmentTypePriceId = 4, EquipmentTypeId = 2, PriceId = 2, Duration = 2 },
                new EquipmentTypePrice { EquipmentTypePriceId = 5, EquipmentTypeId = 2, PriceId = 3, Duration = 0 },
                new EquipmentTypePrice { EquipmentTypePriceId = 6, EquipmentTypeId = 3, PriceId = 2, Duration = 3 },
                new EquipmentTypePrice { EquipmentTypePriceId = 7, EquipmentTypeId = 3, PriceId = 3, Duration = 0 }
            };
        }
        public static List<Equipment> GetEquipment()
        {
            return new List<Equipment>{
                new Equipment { EquipmentId = 1, Name = "Caterpillar bulldozer", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId = 1},
                new Equipment { EquipmentId = 2, Name = "KamAZ truck", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId = 2},
                new Equipment { EquipmentId = 3, Name = "Komatsu crane", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId = 1},
                new Equipment { EquipmentId = 4, Name = "Volvo steamroller", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId = 2},
                new Equipment { EquipmentId = 5, Name = "Bosch jackhammer", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId = 3},
                new Equipment { EquipmentId = 6, Name = "Kalevi vana Traktor", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId = 3, IsVisible = false}
            };
        }
        public static List<RentItem> GetRentItems()
        {
            return new List<RentItem>{
                new RentItem { RentItemId = 1, StartTime = DateTime.Now.AddDays(-3), EndTime = DateTime.Now.AddDays(-1), Days = 2, EquipmentId = 1, RentId = 1, IsVisible = true},
                new RentItem { RentItemId = 2, StartTime = DateTime.Now.AddDays(-5), EndTime = DateTime.Now, EquipmentId = 2, RentId = 2},
                new RentItem { RentItemId = 3, StartTime = DateTime.Now.AddDays(-1), EndTime = DateTime.Now, EquipmentId = 3, RentId = 2},
                new RentItem { RentItemId = 4, StartTime = DateTime.Now.AddDays(-8), EndTime = DateTime.Now.AddDays(-2), EquipmentId = 5, RentId = 3},
                new RentItem { RentItemId = 5, StartTime = DateTime.Now.AddDays(-6), EndTime = DateTime.Now.AddDays(-3), EquipmentId = 5, RentId = 3}
            };
        }

        public static List<Rent> GetRents()
        {
            return new List<Rent>{
                new Rent { RentId = 1, Created = DateTime.Now.AddDays(-1), Status = Status.Confirmed},
                new Rent { RentId = 2, Created = DateTime.Now.AddDays(-1), Status = Status.Confirmed},
                new Rent { RentId = 3, Created = DateTime.Now.AddDays(-1), Status = Status.Paid}
            };
        }
    }
}

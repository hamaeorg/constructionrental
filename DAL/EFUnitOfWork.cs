﻿using DAL.IEF;
using DAL.IRepositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IRepositoryProvider _repositoryProvider;

        public EFUnitOfWork(ApplicationDbContext dataContext, IRepositoryProvider repositoryProvider)
        {
            _repositoryProvider = repositoryProvider;
            _applicationDbContext = dataContext as ApplicationDbContext;
            if (_applicationDbContext == null)
            {
                throw new NullReferenceException("No EF dbcontext found in UOW");
            }
        }

        public IRepository<Equipment> Equipment => GetCustomRepository<IEquipmentRepository>();
        public IEquipmentTypePriceRepository EquipmentTypePrices => GetCustomRepository<IEquipmentTypePriceRepository>();
        public IRepository<EquipmentType> EquipmentTypes => GetEntityRepository<EquipmentType>();
        public IRepository<Price> Prices => GetEntityRepository<Price>();
        public IRentItemsRepository RentItems => GetCustomRepository<IRentItemsRepository>();
        public IRentsRepository Rents => GetCustomRepository<IRentsRepository>();

        public void SaveChanges()
        {
            _applicationDbContext.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _applicationDbContext.SaveChangesAsync();
        }

        public IRepository<TEntity> GetEntityRepository<TEntity>() where TEntity : class
        {
            return _repositoryProvider.GetEntityRepository<TEntity>();
        }

        public TRepositoryInterface GetCustomRepository<TRepositoryInterface>() where TRepositoryInterface : class
        {
            return _repositoryProvider.GetCustomRepository<TRepositoryInterface>();
        }
    }
}

﻿using DAL.IEF;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.IRepositories
{
    public interface IRepositoryFactory
    {
        Func<ApplicationDbContext, object> GetCustomRepositoryFactory<TRepoInterface>() where TRepoInterface : class;
        Func<ApplicationDbContext, object> GetStandardRepositoryFactory<TEntity>() where TEntity : class;

    }
}

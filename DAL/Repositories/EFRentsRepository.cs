﻿using DAL.EF;
using DAL.IRepositories;
using Domain;
using Domain.enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class EFRentsRepository : EFRepository<Rent>, IRentsRepository
    {
        public EFRentsRepository(ApplicationDbContext dataContext) : base(dataContext)
        {
        }

        public List<Rent> FindByStatus(Status status)
        {
            return RepositoryDbSet
                .Include(x => x.RentItems)
                .ThenInclude(x => x.Equipment)
                .ThenInclude(x => x.EquipmentType)
                .Where(x => (Status)status == x.Status)
                .ToList(); ;
        }

        public override List<Rent> All()
        {
            return RepositoryDbSet
                .Include(x => x.RentItems)
                .ThenInclude(x => x.Equipment)
                .ThenInclude(x => x.EquipmentType)
                .ToList();
        }

        public override Rent Find(params object[] id)
        {
            return RepositoryDbSet
                .Include(x => x.RentItems)
                .ThenInclude(x => x.Equipment)
                .ThenInclude(x => x.EquipmentType)
                .SingleOrDefault(x => x.RentId == (long)id[0]);
        }
    }
}


﻿using DAL.EF;
using DAL.IRepositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class EFEquipmentRepository : EFRepository<Equipment>, IEquipmentRepository
    {
        public EFEquipmentRepository(ApplicationDbContext dataContext) : base(dataContext)
        {
        }

        public override List<Equipment> All()
        {
            return RepositoryDbSet
                .Include(x => x.EquipmentType)
                .ToList();
        }
    }
}

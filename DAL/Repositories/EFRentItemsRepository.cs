﻿using DAL.EF;
using DAL.IRepositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class EFRentItemsRepository : EFRepository<RentItem>, IRentItemsRepository
    {
        public EFRentItemsRepository(ApplicationDbContext dataContext) : base(dataContext)
        {
        }

        public List<RentItem> GetAllByRentId(long id)
        {
            return RepositoryDbSet
                .Where(p => p.RentItemId == id)
                .Include(x => x.Equipment)
                .Include(x => x.Equipment.EquipmentType)
                .ToList();
        }

        public override List<RentItem> All()
        {
            return RepositoryDbSet
                .Include(x => x.Equipment)
                .Include(x => x.Equipment.EquipmentType)
                .ToList();
        }

        public override RentItem Find(params object[] id)
        {
            return RepositoryDbSet
                .Include(x => x.Equipment)
                .ThenInclude(x => x.EquipmentType)
                .SingleOrDefault(x => x.RentItemId == (long)id[0]);
        }
    }
}

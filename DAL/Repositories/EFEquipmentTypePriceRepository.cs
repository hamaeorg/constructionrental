﻿using DAL.EF;
using DAL.IRepositories;
using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace DAL.Repositories
{
    public class EFEquipmentTypePriceRepository : EFRepository<EquipmentTypePrice>, IEquipmentTypePriceRepository
    {
        public EFEquipmentTypePriceRepository(ApplicationDbContext dataContext) : base(dataContext)
        {
        }

        public List<EquipmentTypePrice> FindByEquipmentTypeId(long id)
        {
            return RepositoryDbSet
                .Include(x => x.Price)
                .Where(x => id == x.EquipmentTypeId)
                .ToList(); ;
        }

        public override List<EquipmentTypePrice> All()
        {
            return RepositoryDbSet
                .Include(x => x.Price)
                .ToList();
        }

        public override EquipmentTypePrice Find(params object[] id)
        {
            return RepositoryDbSet
                .Include(x => x.Price)
                .SingleOrDefault(x => x.EquipmentTypePriceId == (long)id[0]);
        }

    }
}

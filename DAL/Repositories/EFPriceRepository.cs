﻿using DAL.EF;
using DAL.IRepositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class EFPriceRepository : EFRepository<Price>, IPriceRepository
    {
        public EFPriceRepository(ApplicationDbContext dataContext) : base(dataContext)
        {
        }
    }
}

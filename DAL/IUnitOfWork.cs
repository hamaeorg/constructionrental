﻿using DAL.IEF;
using DAL.IRepositories;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL
{
    public interface IUnitOfWork
    {
        void SaveChanges();
        Task SaveChangesAsync();
        IRepository<TEntity> GetEntityRepository<TEntity>() where TEntity : class;
        TRepositoryInterface GetCustomRepository<TRepositoryInterface>() where TRepositoryInterface : class;
        IRepository<Equipment> Equipment { get; }
        IEquipmentTypePriceRepository EquipmentTypePrices { get; }
        IRepository<EquipmentType> EquipmentTypes { get; }
        IRepository<Price> Prices { get; }
        IRentItemsRepository RentItems { get; }
        IRentsRepository Rents { get; }

    }
}

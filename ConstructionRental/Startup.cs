using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DAL;
using BL.IServices;
using BL.Services;
using BL.IFactories;
using BL.Factories;
using DAL.IRepositories;
using DAL.IEF;
using DAL.EF;
using DAL.Repositories;

namespace ConstructionRental
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase(databaseName: "ConstructionRental"));


            services.AddTransient<IEquipmentService, EquipmentService>();
            services.AddTransient<IEquipmentFactory, EquipmentFactory>();

            services.AddTransient<IEquipmentTypePricesService, EquipmentTypePricesService>();
            services.AddTransient<IEquipmentTypePriceFactory, EquipmentTypePriceFactory>();

            services.AddTransient<IEquipmentTypesService, EquipmentTypesService>();
            services.AddTransient<IEquipmentTypeFactory, EquipmentTypeFactory>();

            services.AddTransient<IInvoiceLinesService, InvoiceLinesService>();

            services.AddTransient<IInvoiceService, InvoiceService>();
            services.AddTransient<IInvoiceFactory, InvoiceFactory>();

            services.AddTransient<IPricesService, PricesService>();
            services.AddTransient<IPriceFactory, PriceFactory>();

            services.AddTransient<IRentItemsService, RentItemsService>();
            services.AddTransient<IRentItemFactory, RentItemFactory>();

            services.AddTransient<IRentsService, RentsService>();
            services.AddTransient<IRentFactory, RentFactory>();

            services.AddSingleton<IRepositoryFactory, EFRepositoryFactory>();
            services.AddScoped<IRepositoryProvider, EFRepositoryProvider>();
            services.AddScoped<ApplicationDbContext>();
            services.AddScoped<IUnitOfWork, EFUnitOfWork>();

            //services.AddControllers();
            services.AddControllersWithViews()
            .AddNewtonsoftJson(options =>
            options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );


            // Add Cors
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ConstructionRental", Version = "v1" });
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ApplicationDbContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ConstructionRental v1"));
                context.Database.EnsureCreated(); // Make sure that database is created with the seed data
            }
            // Enable Cors
            app.UseCors("MyPolicy");

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

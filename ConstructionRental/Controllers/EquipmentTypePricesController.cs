﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.DTO;
using BL.IServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ConstructionRental.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class EquipmentTypePricesController : ControllerBase
    {
        private readonly IEquipmentTypePricesService _equipmentTypePricesService;

        public EquipmentTypePricesController(IEquipmentTypePricesService equipmentTypePricesService)
        {
            _equipmentTypePricesService = equipmentTypePricesService;
        }

        // GET: equipmentTypePrices/
        [HttpGet]
        public List<EquipmentTypePriceDTO> GetAll() => _equipmentTypePricesService.GetAll();

        // GET: equipmentTypePrices/{id}
        [HttpGet("{id:int}")]
        public IActionResult GetById(long id)
        {
            var equipmentTypePrices = _equipmentTypePricesService.GetById(id);
            if (equipmentTypePrices == null) return NotFound("No equipment with id " + id + " found");
            return Ok(equipmentTypePrices);
        }

        // GET: equipmentType/{id:int}/EquipmentTypePrice
        [HttpGet("/EquipmentType/{id:int}/EquipmentTypePrice")]
        public IActionResult GetAllByEquipmentTypeId(long id)
        {
            var equipmentTypePrice = _equipmentTypePricesService.GetByEquipmentTypeId(id);
            if (equipmentTypePrice == null) return NotFound("No EquipmentTypePrices with equipmentTypeId " + id + " found");
            return Ok(equipmentTypePrice);
        }

        // POST: equipmentTypePrices/
        [HttpPost]
        public IActionResult PostEquipmentTypePrice([FromBody] EquipmentTypePriceDTO dto)

        {
            var equipmentTypePrice = _equipmentTypePricesService.Add(dto);
            if (equipmentTypePrice == null) return BadRequest("Vehicle is already in database");
            return Ok(equipmentTypePrice);
        }

        // PUT: equipmentTypePrices/MarkHidden
        [HttpPut]
        [Route("MarkHidden")]
        public IActionResult MarkHidden(int id)
        {
            var equipmentTypePrice = _equipmentTypePricesService.MarkHidden(id);
            if (equipmentTypePrice == null) return BadRequest("EquipmentTypePrice with id " + id + " not found");
            return Ok(equipmentTypePrice);
        }

        // DELETE: equipmentTypePrices/{id}
        [HttpDelete("{id:int}")]
        public IActionResult Delete(long id)
        {
            bool equipmentTypePrices = _equipmentTypePricesService.Delete(id);
            if (equipmentTypePrices == false) return BadRequest("Equipment with id " + id + " not found");
            return Ok(id);

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.IServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ConstructionRental.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class InvoicesController : ControllerBase
    {
        private readonly IInvoiceService _invoiceService;

        public InvoicesController(IInvoiceService invoiceService)
        {
            _invoiceService = invoiceService;
        }
        // GET: /Rents/{id:int}/invoices
        [HttpGet("/Rents/{id:int}/invoices")]
        public IActionResult GenerateInvoiceByRentId(long id)
        {
            var invoice = _invoiceService.GenerateInvoiceByRentId(id);
            if (invoice == null) return NotFound("No invoice found for this rent");
            return Ok(invoice);
        }

    }
}

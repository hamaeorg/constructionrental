﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BL.IServices;
using DAL;
using Domain;
using BL.DTO;
using Domain.enums;

namespace ConstructionRental.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RentsController : ControllerBase
    {
        private readonly IRentsService _rentsService;

        public RentsController(IRentsService rentsService)
        {
            _rentsService = rentsService;
        }


        // GET: rents/{id}
        [HttpGet("/{id:int}")]
        public IActionResult GetById(long id)
        {
            var rent = _rentsService.GetById(id);
            if (rent == null) return NotFound("No rent with id " + id + " found");
            return Ok(rent);
        }

        // GET: rents/{status:string}
        [HttpGet]
        public List<RentDTO> GetAll(string status)
        {
            if (status == null)
            {
                return _rentsService.GetAll();
            }
            var rents = _rentsService.GetByStatus(status);
            return rents;
        }

        // POST: rents/
        [HttpPost]
        public IActionResult PostRent([FromBody] RentDTO dto)

        {
            var rent = _rentsService.GetOrCreateRent(dto);
            if (rent == null) return BadRequest("Rent is already in database");
            return Ok(rent);
        }

        // PUT: rents/{id:int}
        [HttpPut("{id:int}")]
        public IActionResult Put(long id, RentDTO dto)
        {
            var rent = _rentsService.Put(dto);
            if (rent == null) return BadRequest("Rent with id " + id + " not found");
            return Ok(rent);
        }

        // DELETE: rents/{id}
        [HttpDelete("{id:int}")]
        public IActionResult Delete(long id)
        {
            bool rent = _rentsService.Delete(id);
            if (rent == false) return BadRequest("Rent with id " + id + " not found");
            return Ok(id);
        }
    }
}

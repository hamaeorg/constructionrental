﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BL.IServices;
using BL.DTO;

namespace ConstructionRental.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class EquipmentsController : ControllerBase
    {
        private readonly IEquipmentService _equipmentService;

        public EquipmentsController(IEquipmentService equipmentService)
        {
            _equipmentService = equipmentService;
        }

        // GET: equipments/
        [HttpGet]
        public List<EquipmentDTO> GetAll() => _equipmentService.GetAll();

        // GET: equipments/{id}
        [HttpGet("{id:int}")]
        public IActionResult GetById(long id)
        {
            var equipment = _equipmentService.GetById(id);
            if (equipment == null) return NotFound("No equipment with id " + id + " found");
            return Ok(equipment);
        }

        // POST: equipments/
        [HttpPost]
        public IActionResult PostEquipment([FromBody] EquipmentDTO dto)

        {
            var equipment = _equipmentService.Add(dto);
            if (equipment == null) return BadRequest("Vehicle is already in database");
            return Ok(equipment);
        }

        // PUT: equipments/MarkHidden
        [HttpPut]
        [Route("MarkHidden")]
        public IActionResult MarkHidden(int id)
        {
            var equipment = _equipmentService.MarkHidden(id);
            if (equipment == null) return BadRequest("Equipment with id " + id + " not found");
            return Ok(equipment);
        }

        // DELETE: equipments/{id}
        [HttpDelete("{id:int}")]
        public IActionResult Delete(long id)
        {
            bool equipment = _equipmentService.Delete(id);
            if (equipment == false) return BadRequest("Equipment with id " + id + " not found");
            return Ok(id);

        }
    }
}

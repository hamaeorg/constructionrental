﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.DTO;
using BL.IServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ConstructionRental.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RentItemsController : ControllerBase
    {
        private readonly IRentItemsService _rentItemsService;

        public RentItemsController(IRentItemsService rentItemsService)
        {
            _rentItemsService = rentItemsService;
        }

        // GET: rentItems/
        [HttpGet]
        public List<RentItemDTO> GetAll() => _rentItemsService.GetAll();

        // GET: rentItems/{id}
        [HttpGet("{id:int}")]
        public IActionResult GetById(long id)
        {
            var rentItem = _rentItemsService.GetById(id);
            if (rentItem == null) return NotFound("No rentItem with id " + id + " found");
            return Ok(rentItem);
        }

        // GET: rents/{id:int}/rentItems
        [HttpGet("/Rents/{id:int}/rentItems")]
        public IActionResult GetAllByRentId(long id)
        {
            var rentItems = _rentItemsService.GetAllByRentId(id);
            if (rentItems == null) return NotFound("No rentItems with rentId " + id + " found");
            return Ok(rentItems);
        }

        // POST: rentItems/
        [HttpPost]
        public IActionResult PostRentItem([FromBody] RentItemDTO dto)
        {
            var rentItem = _rentItemsService.Add(dto);
            if (rentItem == null) return BadRequest("rentItem is already in database");
            return Ok(rentItem);
        }

        // PUT: rentItems/5
        [HttpPut("{id:int}")]
        public IActionResult Put(long id, RentItemDTO dto)
        {
            var rentItem = _rentItemsService.Put(dto);
            if (rentItem == null) return BadRequest("Rent with id " + id + " not found");
            return Ok(rentItem);
        }

        // DELETE: rentItems/{id}
        [HttpDelete("{id:int}")]
        public IActionResult Delete(long id)
        {
            bool rentItem = _rentItemsService.Delete(id);
            if (rentItem == false) return BadRequest("RentItem with id " + id + " not found");
            return Ok(id);

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Domain
{
    public class Equipment : BaseDomain
    {
        public long EquipmentId { get; set; }
        public string Name { get; set; }
        public bool IsAvailable { get; set; }
        public virtual List<RentItem> RentItems { get; set; } = new List<RentItem>();
        [Required]
        public long EquipmentTypeId { get; set; }
        public virtual EquipmentType EquipmentType { get; set; }
    }
}

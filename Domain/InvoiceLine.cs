﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Domain
{
    public class InvoiceLine : BaseDomain
    {
        public long InvoiceLineId { get; set; }
        public long RentItemId { get; set; }
        public RentItem RentItem { get; set; }
        public virtual List<EquipmentTypePrice> EquipmentTypePrices { get; set; } = new List<EquipmentTypePrice>();
        public decimal Sum { get; set; }
        public int LoyaltyPoints { get; set; } = 0;
    }
}

﻿using Domain.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain
{
    public class EquipmentType : BaseDomain
    {
        public long EquipmentTypeId { get; set; }
        public EqType Type { get; set; }
        public int LoyaltyPoints { get; set; }
        public virtual List<Equipment> Equipment { get; set; } = new List<Equipment>();
        public virtual List<EquipmentTypePrice> EquipmentTypePrices { get; set; } = new List<EquipmentTypePrice>();
    }
}

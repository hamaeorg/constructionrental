﻿using Domain.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Rent : BaseDomain
    {
        public long RentId { get; set; }
        public Status Status { get; set; } = Status.Open;
        public virtual List<RentItem> RentItems { get; set; } = new List<RentItem>();
    }
}

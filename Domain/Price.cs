﻿using Domain.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain
{
    public class Price : BaseDomain
    {
        public long PriceId { get; set; }
        public PriceType Type { get; set; }
        public decimal PriceValue { get; set; }
        public bool IsRecurrent { get; set; }
        public virtual List<EquipmentTypePrice> EquipmentTypePrices { get; set; } = new List<EquipmentTypePrice>();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.enums
{
    public enum PriceType
    {
        OneTime = 1,
        PremiumDaily = 2,
        RegularDaily = 3
    }
}

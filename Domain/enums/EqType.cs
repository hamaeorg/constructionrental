﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.enums
{
    public enum EqType
    {
        Heavy = 1,
        Regular = 2,
        Specialized = 3
    };
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.enums
{
    public enum Status
    {
        Open = 1,
        Confirmed = 2,
        Cancelled = 3,
        Paid = 4
    };
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class BaseDomain
    {
        public DateTime Created { get; set; } = DateTime.Now;
        public bool IsVisible { get; set; } = true;
    }
}

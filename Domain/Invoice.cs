﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain
{
    public class Invoice : BaseDomain
    {
        public long InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public string IBAN { get; set; }
        public string Bank { get; set; }
        public string ReferenceNumber { get; set; }
        public virtual List<InvoiceLine> InvoiceLines { get; set; } = new List<InvoiceLine>();
        public decimal VATrate { get; set; }
        public decimal Price { get; set; }
        public decimal PriceWithVAT { get; set; }
        public int LoyaltyPoints { get; set; } = 0;
    }
}

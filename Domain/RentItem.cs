﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Domain
{
    public class RentItem : BaseDomain
    {
        public long RentItemId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int Days { get; set; }
        [Required]
        public long EquipmentId { get; set; }
        public virtual Equipment Equipment { get; set; }
        public long RentId { get; set; }
        public virtual Rent Rent { get; set; }
    }
}
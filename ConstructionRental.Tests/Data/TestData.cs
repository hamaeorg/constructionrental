﻿using BL.DTO;
using Domain;
using Domain.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConstructionRental.Tests.Data
{
    public static class TestData
    {
        public static List<EquipmentTypeDTO> GetEquipmentTypeDTOs()
        {
            return new List<EquipmentTypeDTO>{
                new EquipmentTypeDTO { EquipmentTypeId = 1, Type = "Heavy", LoyaltyPoints = 2 },
                new EquipmentTypeDTO { EquipmentTypeId = 2, Type = "Regular", LoyaltyPoints = 1 },
                new EquipmentTypeDTO { EquipmentTypeId = 3, Type = "Specialized", LoyaltyPoints = 1 }
            };
        }
        public static List<EquipmentType> GetEquipmentType()
        {
            return new List<EquipmentType>{
                new EquipmentType { EquipmentTypeId = 1, Type = EqType.Heavy, LoyaltyPoints = 2},
                new EquipmentType { EquipmentTypeId = 2, Type = EqType.Regular, LoyaltyPoints = 1},
                new EquipmentType { EquipmentTypeId = 3, Type = EqType.Specialized, LoyaltyPoints = 1}
            };
        }


        public static List<PriceDTO> GetPriceDTOs()
        {
            return new List<PriceDTO>{
                new PriceDTO { PriceId = 1, Type = "OneTime", PriceValue = new decimal(100), IsRecurrent = false},
                new PriceDTO { PriceId = 2, Type = "PremiumDaily", PriceValue = new decimal(60), IsRecurrent = true },
                new PriceDTO { PriceId = 3, Type = "RegularDaily", PriceValue = new decimal(40), IsRecurrent = true }
            };
        }
        public static List<Price> GetPrice()
        {
            return new List<Price>{
                new Price { PriceId = 1, Type = PriceType.OneTime, PriceValue = new decimal(100), IsRecurrent = false },
                new Price { PriceId = 2, Type = PriceType.PremiumDaily, PriceValue = new decimal(60), IsRecurrent = true },
                new Price { PriceId = 3, Type = PriceType.RegularDaily, PriceValue = new decimal(40), IsRecurrent = true }
            };
        }


        public static List<EquipmentTypePriceDTO> GetEquipmentTypePriceDTOs()
        {
            return new List<EquipmentTypePriceDTO>{
                new EquipmentTypePriceDTO { EquipmentTypePriceId = 1, EquipmentTypeId = 1, PriceId = 1, Duration = 0 },
                new EquipmentTypePriceDTO { EquipmentTypePriceId = 2, EquipmentTypeId = 1, PriceId = 2, Duration = 0 },
                new EquipmentTypePriceDTO { EquipmentTypePriceId = 3, EquipmentTypeId = 2, PriceId = 1, Duration = 0 },
                new EquipmentTypePriceDTO { EquipmentTypePriceId = 4, EquipmentTypeId = 2, PriceId = 2, Duration = 2 },
                new EquipmentTypePriceDTO { EquipmentTypePriceId = 5, EquipmentTypeId = 2, PriceId = 3, Duration = 0 },
                new EquipmentTypePriceDTO { EquipmentTypePriceId = 6, EquipmentTypeId = 3, PriceId = 2, Duration = 3 },
                new EquipmentTypePriceDTO { EquipmentTypePriceId = 7, EquipmentTypeId = 3, PriceId = 3, Duration = 0 }
            };
        }
        public static List<EquipmentTypePrice> GetEquipmentTypePrices()
        {
            return new List<EquipmentTypePrice>{
                new EquipmentTypePrice { EquipmentTypePriceId = 1, EquipmentTypeId = 1, PriceId = 1, Duration = 0 },
                new EquipmentTypePrice { EquipmentTypePriceId = 2, EquipmentTypeId = 1, PriceId = 2, Duration = 0 },

                new EquipmentTypePrice { EquipmentTypePriceId = 3, EquipmentTypeId = 2, PriceId = 1, Duration = 0 },
                new EquipmentTypePrice { EquipmentTypePriceId = 4, EquipmentTypeId = 2, PriceId = 2, Duration = 2 },
                new EquipmentTypePrice { EquipmentTypePriceId = 5, EquipmentTypeId = 2, PriceId = 3, Duration = 0 },

                new EquipmentTypePrice { EquipmentTypePriceId = 6, EquipmentTypeId = 3, PriceId = 2, Duration = 3 },
                new EquipmentTypePrice { EquipmentTypePriceId = 7, EquipmentTypeId = 3, PriceId = 3, Duration = 0 }
            };
        }

        public static List<EquipmentTypePriceDTO> GetEquipmentTypePriceDTOsWithEq1()
        {
            return new List<EquipmentTypePriceDTO>{
                new EquipmentTypePriceDTO { EquipmentTypePriceId = 1, EquipmentTypeId = 1, PriceId = 1, Duration = 0, Price = GetPriceDTOs()[0] },
                new EquipmentTypePriceDTO { EquipmentTypePriceId = 2, EquipmentTypeId = 1, PriceId = 2, Duration = 0, Price = GetPriceDTOs()[1] }
            };
        }

        public static List<EquipmentTypePriceDTO> GetEquipmentTypePriceDTOsWithEq2()
        {
            return new List<EquipmentTypePriceDTO>{
                new EquipmentTypePriceDTO { EquipmentTypePriceId = 3, EquipmentTypeId = 2, PriceId = 1, Duration = 0, Price = GetPriceDTOs()[0] },
                new EquipmentTypePriceDTO { EquipmentTypePriceId = 4, EquipmentTypeId = 2, PriceId = 2, Duration = 2, Price = GetPriceDTOs()[1] },
                new EquipmentTypePriceDTO { EquipmentTypePriceId = 5, EquipmentTypeId = 2, PriceId = 3, Duration = 0, Price = GetPriceDTOs()[2] }
                };
        }

        public static List<EquipmentTypePriceDTO> GetEquipmentTypePriceDTOsWithEq3()
        {
            return new List<EquipmentTypePriceDTO>
            {
                new EquipmentTypePriceDTO { EquipmentTypePriceId = 6, EquipmentTypeId = 3, PriceId = 2, Duration = 3, Price = GetPriceDTOs()[1] },
                new EquipmentTypePriceDTO { EquipmentTypePriceId = 7, EquipmentTypeId = 3, PriceId = 3, Duration = 0, Price = GetPriceDTOs()[2] }
            };
        }

        public static List<EquipmentDTO> GetEquipmentDTOs()
        {
            return new List<EquipmentDTO>{
                new EquipmentDTO { EquipmentId = 1, Name = "Caterpillar bulldozer", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId =1, EquipmentType = GetEquipmentTypeDTOs()[0]},
                new EquipmentDTO { EquipmentId = 2, Name = "KamAZ truck", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId = 2},
                new EquipmentDTO { EquipmentId = 3, Name = "Komatsu crane", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId = 1},
                new EquipmentDTO { EquipmentId = 4, Name = "Volvo steamroller", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId = 2},
                new EquipmentDTO { EquipmentId = 5, Name = "Bosch jackhammer", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId = 3},
                new EquipmentDTO { EquipmentId = 6, Name = "Kalevi vana Traktor", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId = 3, IsVisible = false}
            };
        }
        public static List<Equipment> GetEquipment()
        {
            return new List<Equipment>{
                new Equipment { EquipmentId = 1, Name = "Caterpillar bulldozer", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId =1},
                new Equipment { EquipmentId = 2, Name = "KamAZ truck", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId = 2},
                new Equipment { EquipmentId = 3, Name = "Komatsu crane", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId = 1},
                new Equipment { EquipmentId = 4, Name = "Volvo steamroller", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId = 2},
                new Equipment { EquipmentId = 5, Name = "Bosch jackhammer", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId = 3},
                new Equipment { EquipmentId = 6, Name = "Kalevi vana Traktor", Created = DateTime.Now, IsAvailable = true, EquipmentTypeId = 3, IsVisible = false}
            };
        }


        public static List<RentItemDTO> GetRentItemDTOs()
        {
            return new List<RentItemDTO>{
                new RentItemDTO { RentItemId = 1, StartTime = DateTime.Now.AddDays(-3), EndTime = DateTime.Now.AddDays(-1), EquipmentId = 1, RentId = 1},
                new RentItemDTO { RentItemId = 2, StartTime = DateTime.Now.AddDays(-5), EndTime = DateTime.Now, EquipmentId = 2, RentId = 2},
                new RentItemDTO { RentItemId = 3, StartTime = DateTime.Now.AddDays(-1), EndTime = DateTime.Now, EquipmentId = 3, RentId = 2},
                new RentItemDTO { RentItemId = 4, StartTime = DateTime.Now.AddDays(-8), EndTime = DateTime.Now.AddDays(-2), EquipmentId = 5, RentId = 1},
                new RentItemDTO { RentItemId = 5, StartTime = DateTime.Now.AddDays(-6), EndTime = DateTime.Now.AddDays(-3), EquipmentId = 5, RentId = 3}
            };
        }
        public static List<RentItem> GetRentItems()
        {
            return new List<RentItem>{
                new RentItem { RentItemId = 1, StartTime = DateTime.Now.AddDays(-3), EndTime = DateTime.Now.AddDays(-1), EquipmentId = 1, RentId = 1},
                new RentItem { RentItemId = 2, StartTime = DateTime.Now.AddDays(-5), EndTime = DateTime.Now, EquipmentId = 2, RentId = 2},
                new RentItem { RentItemId = 3, StartTime = DateTime.Now.AddDays(-1), EndTime = DateTime.Now, EquipmentId = 3, RentId = 2},
                new RentItem { RentItemId = 4, StartTime = DateTime.Now.AddDays(-8), EndTime = DateTime.Now.AddDays(-2), EquipmentId = 5, RentId = 3},
                new RentItem { RentItemId = 5, StartTime = DateTime.Now.AddDays(-6), EndTime = DateTime.Now.AddDays(-3), EquipmentId = 5, RentId = 3}
            };
        }

        public static RentItemDTO GetRentItemDTOWithEquipment()
        {
            return new RentItemDTO { RentItemId = 1, StartTime = DateTime.Now.AddDays(-6), EndTime = DateTime.Now.AddDays(-1), EquipmentId = 1, RentId = 1, Days = 5, Equipment = GetEquipmentDTOs()[0] };
        }

        public static InvoiceLineDTO GetInvoiceLineDTO()
        {
            return new InvoiceLineDTO { InvoiceLineId = 1, RentItemId = 1, RentItem = GetRentItemDTOWithEquipment(), EquipmentTypePrices = GetEquipmentTypePriceDTOsWithEq1(), Sum = new Decimal(400), LoyaltyPoints = 2 };
                
        }

        public static List<RentDTO> GetRentDTOs()
        {
            return new List<RentDTO>{
                new RentDTO { RentId = 1, Created = DateTime.Now.AddDays(-1), Status = "Confirmed"},
                new RentDTO { RentId = 2, Created = DateTime.Now.AddDays(-1), Status = "Open"},
                new RentDTO { RentId = 3, Created = DateTime.Now.AddDays(-1), Status = "Settled"}
            };
        }
        public static List<Rent> GetRents()
        {
            return new List<Rent>{
                new Rent { RentId = 1, Created = DateTime.Now.AddDays(-1), Status = Status.Confirmed},
                new Rent { RentId = 2, Created = DateTime.Now.AddDays(-1), Status = Status.Open},
                new Rent { RentId = 3, Created = DateTime.Now.AddDays(-1), Status = Status.Paid}
            };
        }


        public static List<InvoiceLineDTO> GetInvoiceLineDTOs()
        {
            return new List<InvoiceLineDTO>{
                new InvoiceLineDTO { InvoiceLineId = 1, RentItemId = 1, Sum = 70},
                new InvoiceLineDTO { InvoiceLineId = 2, RentItemId = 3, Sum = 30},
                new InvoiceLineDTO { InvoiceLineId = 3, RentItemId = 5, Sum = 100}
            };
        }
        public static List<InvoiceDTO> GetInvoiceDTOs()
        {
            return new List<InvoiceDTO>
            {
                 new InvoiceDTO { InvoiceId = 1, InvoiceNumber = "123456", IBAN = "abc123456789", Bank = "SAB", ReferenceNumber = "555555", Created = DateTime.Now, VATrate = 20, Price = 100, PriceWithVAT = 120 },
                 new InvoiceDTO { InvoiceId = 2, InvoiceNumber = "123456", IBAN = "abc123456789", Bank = "SAB", ReferenceNumber = "555555", Created = DateTime.Now, VATrate = 20, Price = 100, PriceWithVAT = 120 }

            };
        }
        public static InvoiceDTO GetBaseInvoiceDTO()
        {
            Random generator = new Random();
            return new InvoiceDTO()
            {
                InvoiceNumber = generator.Next(1, 10000).ToString("D4"),
                IBAN = "abc123456789",
                Bank = "SAB",
                ReferenceNumber = generator.Next(1, 10000).ToString("D4"),
                Created = DateTime.Now,
                VATrate = 20
            };
        }
    }
}

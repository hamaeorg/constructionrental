﻿using BL.DTO;
using BL.IServices;
using ConstructionRental.Controllers;
using ConstructionRental.Tests.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructionRental.Tests.Controllers
{
    [TestClass]
    public class TestEquipmentTypePricesController
    {

        private Mock<IEquipmentTypePricesService> MockService = new Mock<IEquipmentTypePricesService>();
        [TestMethod]
        public void GetAll_ShouldReturnAllEquipment()
        {
            var testEquipmentTypePrices = TestData.GetEquipmentTypePriceDTOs();
            MockService.Setup(p => p.GetAll()).Returns(testEquipmentTypePrices);
            var controller = new EquipmentTypePricesController(MockService.Object);

            var result = controller.GetAll() as List<EquipmentTypePriceDTO>;
            Assert.IsNotNull(result);
            Assert.AreEqual(testEquipmentTypePrices.Count, result.Count);
        }

        [TestMethod]
        public void GetById_ShouldReturnCorrectEquipment()
        {
            var testEquipmentTypePrices = TestData.GetEquipmentTypePriceDTOs();
            var dto = testEquipmentTypePrices[2];
            MockService.Setup(p => p.GetById(3)).Returns(dto);
            var controller = new EquipmentTypePricesController(MockService.Object);

            var result = controller.GetById(3) as OkObjectResult;
            Assert.IsNotNull(result);
            var returnDTO = result.Value as EquipmentTypePriceDTO;
            Assert.AreEqual(testEquipmentTypePrices[2].EquipmentTypePriceId, returnDTO.EquipmentTypePriceId);
        }

        [TestMethod]
        public void PostEquipmentTypePrice_ShouldReturnOkResult()
        {
            var testEquipmentTypePrices = TestData.GetEquipmentTypePriceDTOs();
            var dto = testEquipmentTypePrices[1];
            MockService.Setup(p => p.Add(dto)).Returns(dto);
            var controller = new EquipmentTypePricesController(MockService.Object);

            var result = controller.PostEquipmentTypePrice(dto) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);
        }

        [TestMethod]
        public void Delete_ShouldReturnOkResult()
        {
            MockService.Setup(p => p.Delete(1)).Returns(true);
            var controller = new EquipmentTypePricesController(MockService.Object);

            var result = controller.Delete(1) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);
        }

        [TestMethod]
        public void GetById_ShouldReturnNotFound()
        {
            MockService.Setup(p => p.GetById(99)).Returns((EquipmentTypePriceDTO)null);
            var controller = new EquipmentTypePricesController(MockService.Object);

            var result = controller.GetById(99) as NotFoundObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(404, result.StatusCode);
        }

        [TestMethod]
        public void Delete_ShouldReturnBadRequest()
        {
            MockService.Setup(p => p.Delete(99)).Returns(false);
            var controller = new EquipmentTypePricesController(MockService.Object);

            var result = controller.Delete(99) as BadRequestObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(400, result.StatusCode);
        }
    }
}

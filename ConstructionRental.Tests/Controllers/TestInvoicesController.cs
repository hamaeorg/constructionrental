﻿using Domain;
using DAL.Helpers;
using BL.IServices;
using ConstructionRental.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstructionRental.Tests.Data;
using BL.DTO;

namespace ConstructionRental.Tests.Controllers
{
    [TestClass]
    public class TestInvoicesController
    {
        private Mock<IInvoiceService> MockService = new Mock<IInvoiceService>();
        [TestMethod]
        public void GetInvoice_ShouldReturnInvoice()
        {
            var testInvoice = TestData.GetInvoiceDTOs()[1];
            MockService.Setup(p => p.GenerateInvoiceByRentId(1)).Returns(testInvoice);
            var controller = new InvoicesController(MockService.Object);

            var result = controller.GenerateInvoiceByRentId(1) as OkObjectResult;
            Assert.IsNotNull(result);
            var returnDTO = result.Value as InvoiceDTO;
            Assert.AreEqual(testInvoice.PriceWithVAT, testInvoice.PriceWithVAT);
        }
    }
}

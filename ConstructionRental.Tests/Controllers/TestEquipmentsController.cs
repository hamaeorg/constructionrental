﻿using BL.DTO;
using BL.IServices;
using ConstructionRental.Controllers;
using ConstructionRental.Tests.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructionRental.Tests.Controllers
{
    [TestClass]
    public class TestEquipmentsController
    {
        private Mock<IEquipmentService> MockService = new Mock<IEquipmentService>();

        [TestMethod]
        public void GetAll_ShouldReturnAllEquipment()
        {
            var testEquipment = TestData.GetEquipmentDTOs();
            MockService.Setup(p => p.GetAll()).Returns(testEquipment);
            var controller = new EquipmentsController(MockService.Object);

            var result = controller.GetAll() as List<EquipmentDTO>;
            Assert.IsNotNull(result);
            Assert.AreEqual(testEquipment.Count, result.Count);
        }

        [TestMethod]
        public void GetById_ShouldReturnCorrectEquipment()
        {
            var testEquipment = TestData.GetEquipmentDTOs();
            var dto = testEquipment[2];
            MockService.Setup(p => p.GetById(3)).Returns(dto);
            var controller = new EquipmentsController(MockService.Object);

            var result = controller.GetById(3) as OkObjectResult;
            Assert.IsNotNull(result);
            var returnDTO = result.Value as EquipmentDTO;
            Assert.AreEqual(testEquipment[2].Name, returnDTO.Name);
        }

        [TestMethod]
        public void PostEquipment_ShouldReturnOkResult()
        {
            var testEquipment = TestData.GetEquipmentDTOs();
            var dto = testEquipment[1];
            MockService.Setup(p => p.Add(dto)).Returns(dto);
            var controller = new EquipmentsController(MockService.Object);

            var result = controller.PostEquipment(dto) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);
        }

        [TestMethod]
        public void Delete_ShouldReturnOkResult()
        {
            MockService.Setup(p => p.Delete(1)).Returns(true);
            var controller = new EquipmentsController(MockService.Object);

            var result = controller.Delete(1) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);
        }

        [TestMethod]
        public void GetById_ShouldReturnNotFound()
        {
            MockService.Setup(p => p.GetById(99)).Returns((EquipmentDTO)null);
            var controller = new EquipmentsController(MockService.Object);

            var result = controller.GetById(99) as NotFoundObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(404, result.StatusCode);
        }

        [TestMethod]
        public void Delete_ShouldReturnBadRequest()
        {
            MockService.Setup(p => p.Delete(99)).Returns(false);
            var controller = new EquipmentsController(MockService.Object);

            var result = controller.Delete(99) as BadRequestObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(400, result.StatusCode);
        }
    }
}

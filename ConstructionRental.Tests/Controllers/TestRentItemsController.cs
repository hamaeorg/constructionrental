﻿using BL.DTO;
using BL.IServices;
using ConstructionRental.Controllers;
using ConstructionRental.Tests.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructionRental.Tests.Controllers
{
    [TestClass]
    public class TestRentItemsController
    {
        private Mock<IRentItemsService> MockService = new Mock<IRentItemsService>();
        [TestMethod]
        public void GetAll_ShouldReturnAllRentItems()
        {
            var testRentItems = TestData.GetRentItemDTOs();
            MockService.Setup(p => p.GetAll()).Returns(testRentItems);
            var controller = new RentItemsController(MockService.Object);

            var result = controller.GetAll() as List<RentItemDTO>;
            Assert.IsNotNull(result);
            Assert.AreEqual(testRentItems.Count, result.Count);
        }

        [TestMethod]
        public void GetById_ShouldReturnCorrectRentItem()
        {
            var testRentItems = TestData.GetRentItemDTOs();
            var dto = testRentItems[2];
            MockService.Setup(p => p.GetById(3)).Returns(dto);
            var controller = new RentItemsController(MockService.Object);

            var result = controller.GetById(3) as OkObjectResult;
            Assert.IsNotNull(result);
            var returnDTO = result.Value as RentItemDTO;
            Assert.AreEqual(testRentItems[2].RentItemId, returnDTO.RentItemId);
        }

        [TestMethod]
        public void PostRentItem_ShouldReturnOkResultTypes()
        {
            var dto = TestData.GetRentItemDTOs()[2];
            MockService.Setup(p => p.Add(dto)).Returns(dto);
            var controller = new RentItemsController(MockService.Object);

            var result = controller.PostRentItem(dto) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);
        }

        [TestMethod]
        public void Delete_ShouldReturnOkResult()
        {
            MockService.Setup(p => p.Delete(1)).Returns(true);
            var controller = new RentItemsController(MockService.Object);

            var result = controller.Delete(1) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);
        }

        [TestMethod]
        public void GetById_ShouldReturnNotFound()
        {
            MockService.Setup(p => p.GetById(99)).Returns((RentItemDTO)null);
            var controller = new RentItemsController(MockService.Object);

            var result = controller.GetById(99) as NotFoundObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(404, result.StatusCode);
        }

        [TestMethod]
        public void Delete_ShouldReturnBadRequest()
        {
            MockService.Setup(p => p.Delete(99)).Returns(false);
            var controller = new RentItemsController(MockService.Object);

            var result = controller.Delete(99) as BadRequestObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(400, result.StatusCode);
        }
    }
}

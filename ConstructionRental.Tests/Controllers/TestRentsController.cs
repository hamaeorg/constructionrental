﻿using BL.DTO;
using BL.IServices;
using ConstructionRental.Controllers;
using ConstructionRental.Tests.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructionRental.Tests.Controllers
{
    [TestClass]
    public class TestRentsController
    {
        private Mock<IRentsService> MockService = new Mock<IRentsService>();
        [TestMethod]
        public void GetAllByStatus_ShouldReturnCorrectList()
        {
            var testRent = new List<RentDTO> { TestData.GetRentDTOs()[1] };
            MockService.Setup(p => p.GetByStatus("Open")).Returns(testRent);
            var controller = new RentsController(MockService.Object);

            var result = controller.GetAll("Open");
            Assert.IsNotNull(result);
            Assert.AreEqual(testRent, result);
        }

        [TestMethod]
        public void GetById_ShouldReturnCorrectRentItem()
        {
            var testRentItem = TestData.GetRentDTOs()[1];
            MockService.Setup(p => p.GetById(2)).Returns(testRentItem);
            var controller = new RentsController(MockService.Object);

            var result = controller.GetById(2) as OkObjectResult;
            Assert.IsNotNull(result);
            var returnDTO = result.Value as RentDTO;
            Assert.AreEqual(testRentItem.RentId, returnDTO.RentId);
        }

        [TestMethod]
        public void PostRent_ShouldReturnOkResultTypes()
        {
            var dto = TestData.GetRentDTOs()[2];
            MockService.Setup(p => p.GetOrCreateRent(dto)).Returns(dto);
            var controller = new RentsController(MockService.Object);

            var result = controller.PostRent(dto) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);
        }

        [TestMethod]
        public void Delete_ShouldReturnOkResult()
        {
            MockService.Setup(p => p.Delete(1)).Returns(true);
            var controller = new RentsController(MockService.Object);

            var result = controller.Delete(1) as OkObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(200, result.StatusCode);
        }

        [TestMethod]
        public void GetByWrongId_ShouldReturnNotFound()
        {
            MockService.Setup(p => p.GetById(99)).Returns((RentDTO)null);
            var controller = new RentsController(MockService.Object);

            var result = controller.GetById(99) as NotFoundObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(404, result.StatusCode);
        }

        [TestMethod]
        public void DeleteByWrongId_ShouldReturnBadRequest()
        {
            MockService.Setup(p => p.Delete(99)).Returns(false);
            var controller = new RentsController(MockService.Object);

            var result = controller.Delete(99) as BadRequestObjectResult;
            Assert.IsNotNull(result);
            Assert.AreEqual(400, result.StatusCode);
        }
    }
}

﻿using BL.IFactories;
using BL.IServices;
using BL.Services;
using ConstructionRental.Tests.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructionRental.Tests.Services
{
    [TestClass]
    public class TestInvoiceService
    {

        private Mock<IInvoiceLinesService> MockInvoicLinesService = new Mock<IInvoiceLinesService>();
        private Mock<IRentsService> MockRentsService = new Mock<IRentsService>();
        private Mock<IInvoiceFactory> MockInvoiceFactory = new Mock<IInvoiceFactory>();

        [TestMethod]
        public void PercentCalculator_ShouldReturnRightSum()
        {
            var service = new InvoiceService(MockInvoicLinesService.Object, MockRentsService.Object, MockInvoiceFactory.Object);
            var result = service.PercentCalculator(30, 100);
            Assert.IsNotNull(result);
            Assert.AreEqual(130, result);
        }

        [TestMethod]
        public void CalculateInvoice_ShouldReturnSumOfInvoicelines()
        {
            var invoiceLines = TestData.GetInvoiceLineDTOs();
            var baseinvoice = TestData.GetBaseInvoiceDTO();
            var service = new InvoiceService(MockInvoicLinesService.Object, MockRentsService.Object, MockInvoiceFactory.Object);
            var result = service.CalculateInvoice(baseinvoice, invoiceLines);
            Assert.IsNotNull(result);
            Assert.AreEqual(240, result.PriceWithVAT);
        }
    }
}

﻿using BL.IServices;
using BL.Services;
using ConstructionRental.Tests.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructionRental.Tests.Services
{
    [TestClass]
    public class TestInvoiceLinesService
    {
        private Mock<IEquipmentTypePricesService> MockService = new Mock<IEquipmentTypePricesService>();


        [TestMethod]
        public void GenerateInvoiceLineFromRentItem_ShouldReturnRightInvoiceLine()
        {
            var rentItem = TestData.GetRentItemDTOWithEquipment();
            var eqTypePrices = TestData.GetEquipmentTypePriceDTOsWithEq1();
            MockService.Setup(p => p.GetByEquipmentTypeId(1)).Returns(eqTypePrices);

            var service = new InvoiceLinesService(MockService.Object);
            var result = service.GenerateInvoiceLineFromRentItem(rentItem);
            var invoiceLine = TestData.GetInvoiceLineDTO();
            Assert.IsNotNull(result);
            Assert.AreEqual(invoiceLine.Sum, result.Sum);
            Assert.AreEqual(invoiceLine.RentItemId, result.RentItemId);
            Assert.AreEqual(invoiceLine.RentItem.Equipment.Name, result.RentItem.Equipment.Name);
            Assert.AreEqual(invoiceLine.LoyaltyPoints, result.LoyaltyPoints);
        }

        [TestMethod]
        public void CalculateInvoiceLineSumType1For5Days_ShouldReturnRightSum()
        {
            var eqTypePrices = TestData.GetEquipmentTypePriceDTOsWithEq1();
            var service = new InvoiceLinesService(MockService.Object);
            var result = service.calculateInvoiceLineSum(eqTypePrices, 5);
            Assert.IsNotNull(result);
            Assert.AreEqual(new decimal(400), result);
        }

        [TestMethod]
        public void CalculateInvoiceLineSumForType2For5Days_ShouldReturnRightSum()
        {
            var eqTypePrices = TestData.GetEquipmentTypePriceDTOsWithEq2();
            var service = new InvoiceLinesService(MockService.Object);
            var result = service.calculateInvoiceLineSum(eqTypePrices, 5);
            Assert.IsNotNull(result);
            Assert.AreEqual(new decimal(340), result);
        }

        [TestMethod]
        public void CalculateInvoiceLineSumForType2For2Days_ShouldReturnRightSum()
        {
            var eqTypePrices = TestData.GetEquipmentTypePriceDTOsWithEq2();
            var service = new InvoiceLinesService(MockService.Object);
            var result = service.calculateInvoiceLineSum(eqTypePrices, 2);
            Assert.IsNotNull(result);
            Assert.AreEqual(new decimal(220), result);
        }

        [TestMethod]
        public void CalculateInvoiceLineSumType3For5Days_ShouldReturnRightSum()
        {
            var eqTypePrices = TestData.GetEquipmentTypePriceDTOsWithEq3();
            var service = new InvoiceLinesService(MockService.Object);
            var result = service.calculateInvoiceLineSum(eqTypePrices, 5);
            Assert.IsNotNull(result);
            Assert.AreEqual(new decimal(260), result);
        }
    }
}

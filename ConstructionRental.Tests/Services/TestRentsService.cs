﻿using BL.DTO;
using BL.IFactories;
using BL.Services;
using ConstructionRental.Tests.Data;
using DAL;
using DAL.EF;
using DAL.IEF;
using DAL.IRepositories;
using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructionRental.Tests.Services
{
    [TestClass]
    public class TestRentsService
    {
        private Mock<IUnitOfWork> MockUow = new Mock<IUnitOfWork>();
        private Mock<IRentFactory> MockFactory = new Mock<IRentFactory>();

        [TestMethod]
        public void GetAll_ShouldReturnAllRents()
        {
            var testRentDTOs = TestData.GetRentDTOs();
            var testRents = TestData.GetRents();
            MockUow.Setup(p => p.Rents.All()).Returns(testRents);
            for (int i = 0; i < testRentDTOs.Count; i++)
            {
                MockFactory.Setup(p => p.TransformWithRentItems(testRents[i])).Returns(testRentDTOs[0]);
            }

            var service = new RentsService(MockUow.Object, MockFactory.Object);
            var result = service.GetAll() as List<RentDTO>;
            Assert.IsNotNull(result);
            Assert.AreEqual(testRents.Count, result.Count);
        }
    }
}

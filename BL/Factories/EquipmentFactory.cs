﻿using BL.DTO;
using BL.IFactories;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.Factories
{
    public class EquipmentFactory : IEquipmentFactory
    {
        private readonly IEquipmentTypeFactory _equipmentTypeFactory;
        public EquipmentFactory(IEquipmentTypeFactory equipmentTypeFactory)
        {
            _equipmentTypeFactory = equipmentTypeFactory;
        }
        public EquipmentDTO Transform(Equipment e)
        {
            if (e == null) return null;
            return new EquipmentDTO
            {
                EquipmentId = e.EquipmentId,
                Name = e.Name,
                IsAvailable = e.IsAvailable,
                EquipmentTypeId = e.EquipmentTypeId,
                EquipmentType = _equipmentTypeFactory.Transform(e.EquipmentType),
                Created = e.Created,
                IsVisible = e.IsVisible
            };
        }

        public Equipment Transform(EquipmentDTO dto)
        {
            if (dto == null) return null;
            return new Equipment
            {
                EquipmentId = dto.EquipmentId,
                Name = dto.Name,
                IsAvailable = dto.IsAvailable,
                EquipmentTypeId = dto.EquipmentTypeId,
                EquipmentType = _equipmentTypeFactory.Transform(dto.EquipmentType),
                Created = dto.Created,
                IsVisible = dto.IsVisible
            };
        }

        //public EquipmentDTO TransformWithRents(Equipment e)
        //{
        //    var dto = Transform(e);
        //    if (dto == null) return null;
        //    dto.Rents = e?.Rents
        //        .Select(c => _rentFactory.Transform(c)).ToList();
        //    return dto;
        //}
    }
}

﻿using BL.DTO;
using BL.IFactories;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.Factories
{
    public class EquipmentTypePriceFactory : IEquipmentTypePriceFactory
    {

        private readonly IPriceFactory _priceFactory;
        private readonly IEquipmentTypeFactory _equipmentTypeFactory;
        public EquipmentTypePriceFactory(IPriceFactory priceFactory, IEquipmentTypeFactory equipmentTypeFactory)
        {
            _priceFactory = priceFactory;
            _equipmentTypeFactory = equipmentTypeFactory;
        }

        public EquipmentTypePriceDTO Transform(EquipmentTypePrice e)
        {
            if (e == null) return null;
            return new EquipmentTypePriceDTO()
            {
                EquipmentTypePriceId = e.EquipmentTypePriceId,
                EquipmentTypeId = e.EquipmentTypeId,
                EquipmentType = _equipmentTypeFactory.Transform(e.EquipmentType),
                PriceId = e.PriceId,
                Price = _priceFactory.Transform(e.Price),
                Duration = e.Duration,
                Created = e.Created,
                IsVisible = e.IsVisible
            };
        }

        public EquipmentTypePrice Transform(EquipmentTypePriceDTO dto)
        {
            if (dto == null) return null;
            return new EquipmentTypePrice()
            {
                EquipmentTypePriceId = dto.EquipmentTypePriceId,
                EquipmentTypeId = dto.EquipmentTypeId,
                EquipmentType = _equipmentTypeFactory.Transform(dto.EquipmentType),
                PriceId = dto.PriceId,
                Price = _priceFactory.Transform(dto.Price),
                Duration = dto.Duration,
                Created = dto.Created,
                IsVisible = dto.IsVisible
            };
        }
    }
}

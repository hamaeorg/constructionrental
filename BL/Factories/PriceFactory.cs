﻿using BL.DTO;
using BL.IFactories;
using Domain;
using Domain.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.Factories
{
    public class PriceFactory : IPriceFactory
    {
        public PriceDTO Transform(Price e)
        {
            if (e == null) return null;
            return new PriceDTO()
            {
                PriceId = e.PriceId,
                Type = e.Type.ToString(),
                PriceValue = e.PriceValue,
                Created = e.Created,
                IsRecurrent = e.IsRecurrent,
                IsVisible = e.IsVisible
            };
        }

        public Price Transform(PriceDTO dto)
        {
            if (dto == null) return null;
            return new Price()
            {
                PriceId = dto.PriceId,
                Type = (PriceType)Enum.Parse(typeof(PriceType), dto.Type),
                PriceValue = dto.PriceValue,
                Created = dto.Created,
                IsRecurrent = dto.IsRecurrent,
                IsVisible = dto.IsVisible
            };
        }
    }
}

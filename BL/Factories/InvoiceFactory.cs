﻿using BL.DTO;
using BL.IFactories;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.Factories
{
    public class InvoiceFactory : IInvoiceFactory
    {
        public InvoiceDTO GetInvoiceBase()
        {

            Random generator = new Random();
            return new InvoiceDTO()
            {
                InvoiceNumber = generator.Next(1, 10000).ToString("D4"),
                IBAN = "abc123456789",
                Bank = "SAB",
                ReferenceNumber = generator.Next(1, 10000).ToString("D4"),
                Created = DateTime.Now,
                VATrate = 20
            };
        }
    }
}

﻿using BL.DTO;
using BL.IFactories;
using Domain;
using Domain.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.Factories
{
    public class EquipmentTypeFactory : IEquipmentTypeFactory
    {
        public EquipmentTypeDTO Transform(EquipmentType e)
        {
            if (e == null) return null;
            return new EquipmentTypeDTO()
            {
                EquipmentTypeId = e.EquipmentTypeId,
                Type = e.Type.ToString(),
                LoyaltyPoints = e.LoyaltyPoints,
                Created = e.Created,
                IsVisible = e.IsVisible

            };
        }

        public EquipmentType Transform(EquipmentTypeDTO dto)
        {
            if (dto == null) return null;
            return new EquipmentType()
            {
                EquipmentTypeId = dto.EquipmentTypeId,
                Type = (EqType)Enum.Parse(typeof(EqType), dto.Type),
                LoyaltyPoints = dto.LoyaltyPoints,
                Created = dto.Created,
                IsVisible = dto.IsVisible
            };
        }
    }
}

﻿using BL.DTO;
using BL.IFactories;
using Domain;
using Domain.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Factories
{
    public class RentFactory : IRentFactory
    {

        private readonly IRentItemFactory _rentFactory;
        public RentFactory(IRentItemFactory rentFactory)
        {
            _rentFactory = rentFactory;
        }
        public RentDTO Transform(Rent e)
        {
            if (e == null) return null;
            return new RentDTO()
            {
                RentId = e.RentId,
                Status = e.Status.ToString(),
                Created = e.Created,
                IsVisible = e.IsVisible
            };
        }

        public Rent Transform(RentDTO dto)
        {
            if (dto == null) return null;
            return new Rent()
            {
                RentId = dto.RentId,
                Status = (Status)Enum.Parse(typeof(Status), dto.Status),
                Created = dto.Created,
                IsVisible = dto.IsVisible
            };
        }

        public RentDTO TransformWithRentItems(Rent e)
        {
            var dto = Transform(e);
            if (dto == null) return null;
            dto.RentItems = e?.RentItems
                .Where(p => p.IsVisible == true)
                .Select(c => _rentFactory.Transform(c)).ToList();
            return dto;
        }
    }
}

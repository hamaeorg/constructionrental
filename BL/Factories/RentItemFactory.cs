﻿using BL.DTO;
using BL.IFactories;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.Factories
{
    public class RentItemFactory : IRentItemFactory
    {
        private readonly IEquipmentFactory _equipmentFactory;
        public RentItemFactory(IEquipmentFactory equipmentFactory)
        {
            _equipmentFactory = equipmentFactory;
        }

        public RentItemDTO Transform(RentItem e)
        {
            if (e == null) return null;
            var rentItem = new RentItemDTO()
            {
                RentItemId = e.RentItemId,
                StartTime = e.StartTime,
                EndTime = e.EndTime,
                EquipmentId = e.EquipmentId,
                Equipment = _equipmentFactory.Transform(e.Equipment),
                RentId = e.RentId,
                Created = e.Created,
                IsVisible = e.IsVisible
            };
            rentItem.Days = rentItem.GetDays();
            return rentItem;
        }

        public RentItem Transform(RentItemDTO dto)
        {
            if (dto == null) return null;
            return new RentItem()
            {
                RentItemId = dto.RentItemId,
                StartTime = dto.StartTime,
                EndTime = dto.EndTime,
                EquipmentId = dto.EquipmentId,
                RentId = dto.RentId,
                Created = dto.Created,
                IsVisible = dto.IsVisible
            };
        }
    }
}

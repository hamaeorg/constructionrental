﻿using BL.DTO;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.IServices
{
    public interface IRentItemsService
    {
        List<RentItemDTO> GetAll();
        RentItemDTO GetById(long id);
        List<RentItemDTO> GetAllByEquipmentId(long id);
        List<RentItemDTO> GetAllByRentId(long id);
        RentItemDTO Add(RentItemDTO rent);
        RentItemDTO Put(RentItemDTO dto);
        bool Delete(long id);
    }
}

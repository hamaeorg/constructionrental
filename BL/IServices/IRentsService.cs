﻿using BL.DTO;
using Domain.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.IServices
{
    public interface IRentsService
    {
        List<RentDTO> GetAll();
        RentDTO GetById(long id);
        RentDTO GetOrCreateRent(RentDTO dto);
        List<RentDTO> GetByStatus(string status);

        RentDTO Put(RentDTO dto);
        bool Delete(long id);
    }
}

﻿using BL.DTO;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.IServices
{
    public interface IEquipmentService
    {
        List<EquipmentDTO> GetAll();
        EquipmentDTO GetById(long id);
        EquipmentDTO Add(EquipmentDTO dto);
        EquipmentDTO MarkHidden(long id);
        bool Delete(long id);
    }
}

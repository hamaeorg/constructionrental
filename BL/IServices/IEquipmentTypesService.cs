﻿using BL.DTO;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.IServices
{
    public interface IEquipmentTypesService
    {
        List<EquipmentTypeDTO> GetAll();
        EquipmentTypeDTO GetById(long id);
        EquipmentTypeDTO Add(EquipmentTypeDTO dto);
        EquipmentTypeDTO MarkHidden(long id);
        bool Delete(long id);
    }
}

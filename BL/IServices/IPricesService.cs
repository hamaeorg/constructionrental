﻿using BL.DTO;
using Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.IServices
{
    public interface IPricesService
    {
        List<PriceDTO> GetAll();
        PriceDTO GetById(long id);
        PriceDTO Add(PriceDTO dto);
        PriceDTO MarkHidden(long id);
        bool Delete(long id);
    }
}

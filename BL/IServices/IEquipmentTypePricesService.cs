﻿using BL.DTO;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.IServices
{
    public interface IEquipmentTypePricesService
    {
        List<EquipmentTypePriceDTO> GetAll();
        EquipmentTypePriceDTO GetById(long id);
        List<EquipmentTypePriceDTO> GetByEquipmentTypeId(long id);
        EquipmentTypePriceDTO Add(EquipmentTypePriceDTO dto);
        EquipmentTypePriceDTO MarkHidden(long id);
        bool Delete(long id);
    }
}

﻿using BL.DTO;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.IFactories
{
    public interface IInvoiceFactory
    {
        InvoiceDTO GetInvoiceBase();
    }
}

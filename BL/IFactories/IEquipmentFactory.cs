﻿using BL.DTO;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.IFactories
{
    public interface IEquipmentFactory
    {
        EquipmentDTO Transform(Equipment e);
        Equipment Transform(EquipmentDTO dto);
        //EquipmentDTO TransformWithRents(Equipment e);
    }
}

﻿using BL.DTO;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.IFactories
{
    public interface IRentFactory
    {
        RentDTO Transform(Rent e);
        Rent Transform(RentDTO dto);
        RentDTO TransformWithRentItems(Rent e);
    }
}

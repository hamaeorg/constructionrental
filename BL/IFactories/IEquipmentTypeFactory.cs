﻿using BL.DTO;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.IFactories
{
    public interface IEquipmentTypeFactory
    {
        EquipmentTypeDTO Transform(EquipmentType e);
        EquipmentType Transform(EquipmentTypeDTO dto);
        //EquipmentTypeDTO TransformWithEquipment(EquipmentType e);
        //EquipmentTypeDTO TransformWithEquipmentTypePrices(EquipmentType e);
    }
}

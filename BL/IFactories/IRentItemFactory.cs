﻿using BL.DTO;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.IFactories
{
    public interface IRentItemFactory
    {
        RentItemDTO Transform(RentItem e);
        RentItem Transform(RentItemDTO dto);
    }
}

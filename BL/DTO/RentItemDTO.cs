﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class RentItemDTO : BaseDomain
    {
        public long RentItemId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int Days { get; set; }
        [Required]
        public long EquipmentId { get; set; }
        public virtual EquipmentDTO Equipment { get; set; }
        public long RentId { get; set; }
        public virtual RentDTO Rent { get; set; }

        public int GetDays()
        {
            return (int)(this.EndTime - this.StartTime).TotalDays;
        }
    }
}
﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class EquipmentTypePriceDTO : BaseDomain
    {
        public long EquipmentTypePriceId { get; set; }
        [Required]
        public long EquipmentTypeId { get; set; }
        public virtual EquipmentTypeDTO EquipmentType { get; set; }
        [Required]
        public long PriceId { get; set; }
        public virtual PriceDTO Price { get; set; }
        public int Duration { get; set; }
    }
}

﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class EquipmentTypeDTO : BaseDomain
    {
        public long EquipmentTypeId { get; set; }
        public string Type { get; set; }
        public int LoyaltyPoints { get; set; }
        public virtual List<EquipmentDTO> Equipment { get; set; } = new List<EquipmentDTO>();
        public virtual List<EquipmentTypePriceDTO> EquipmentTypePrices { get; set; } = new List<EquipmentTypePriceDTO>();
    }
}

﻿using Domain;
using Domain.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class RentDTO : BaseDomain
    {
        public long RentId { get; set; }
        public string Status { get; set; } = "Open";
        public virtual List<RentItemDTO> RentItems { get; set; } = new List<RentItemDTO>();
    }
}

﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class PriceDTO : BaseDomain
    {
        public long PriceId { get; set; }
        public string Type { get; set; }
        public decimal PriceValue { get; set; }
        public bool IsRecurrent { get; set; }
        public virtual List<EquipmentTypePriceDTO> EquipmentTypePrices { get; set; } = new List<EquipmentTypePriceDTO>();
    }
}

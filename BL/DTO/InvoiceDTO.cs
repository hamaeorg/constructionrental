﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class InvoiceDTO : BaseDomain
    {
        public long InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public string IBAN { get; set; }
        public string Bank { get; set; }
        public string ReferenceNumber { get; set; }
        public virtual List<InvoiceLineDTO> InvoiceLines { get; set; } = new List<InvoiceLineDTO>();
        public decimal VATrate { get; set; }
        public decimal Price { get; set; }
        public decimal PriceWithVAT { get; set; }
        public int LoyaltyPoints { get; set; } = 0;
    }
}

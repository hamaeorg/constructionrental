﻿using BL.DTO;
using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class InvoiceLineDTO : BaseDomain
    {
        public long InvoiceLineId { get; set; }
        public long RentItemId { get; set; }
        public RentItemDTO RentItem { get; set; }
        public virtual List<EquipmentTypePriceDTO> EquipmentTypePrices { get; set; } = new List<EquipmentTypePriceDTO>();
        public decimal Sum { get; set; }
        public int LoyaltyPoints { get; set; } = 0;
    }
}

﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BL.DTO
{
    public class EquipmentDTO : BaseDomain
    {
        public long EquipmentId { get; set; }
        public string Name { get; set; }
        [Required]
        public bool IsAvailable { get; set; }
        public long EquipmentTypeId { get; set; }
        public virtual EquipmentTypeDTO EquipmentType { get; set; }
        public virtual List<RentItemDTO> RentItems { get; set; } = new List<RentItemDTO>();
    }
}

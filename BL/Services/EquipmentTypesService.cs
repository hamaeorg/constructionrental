﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.IServices;
using BL.IFactories;
using BL.DTO;
using DAL;

namespace BL.Services
{
    public class EquipmentTypesService : IEquipmentTypesService
    {
        private readonly IUnitOfWork _uow;
        private readonly IEquipmentService _equipmentService;
        private readonly IEquipmentTypePricesService _equipmentTypePricesService;
        private readonly IEquipmentTypeFactory _equipmentTypeFactory;
        public EquipmentTypesService(IUnitOfWork uow, IEquipmentService equipmentService, IEquipmentTypePricesService equipmentTypePricesService, IEquipmentTypeFactory equipmentTypeFactory)
        {
            _uow = uow;
            _equipmentService = equipmentService;
            _equipmentTypePricesService = equipmentTypePricesService;
            _equipmentTypeFactory = equipmentTypeFactory;
        }

        public EquipmentTypeDTO Add(EquipmentTypeDTO dto)
        {
            var equipmentType = _equipmentTypeFactory.Transform(dto);
            _uow.EquipmentTypes.Add(equipmentType);
            _uow.SaveChanges();
            return _equipmentTypeFactory.Transform(equipmentType);
        }

        public List<EquipmentTypeDTO> GetAll()
        {
            return _uow.EquipmentTypes.All().Where(p => p.IsVisible == true).Select(e => _equipmentTypeFactory.Transform(e)).ToList();
        }

        public EquipmentTypeDTO GetById(long id)
        {
            var equipmentType = _uow.EquipmentTypes.Find(id);
            if (equipmentType == null || equipmentType.IsVisible == false)
            {
                return null;
            }
            return _equipmentTypeFactory.Transform(equipmentType);
        }
        public EquipmentTypeDTO MarkHidden(long id)
        {
            var equipmentType = _uow.EquipmentTypes.Find(id);
            if (equipmentType == null)
            {
                return null;
            }
            _uow.EquipmentTypes.Detach(equipmentType);
            equipmentType.IsVisible = false;
            _uow.EquipmentTypes.Update(equipmentType);
            _uow.SaveChanges();
            return _equipmentTypeFactory.Transform(equipmentType);
        }
        public bool Delete(long id)
        {
            var equipmentType = _uow.EquipmentTypes.Find(id);
            if (equipmentType == null)
            {
                return false;
            }
            _uow.EquipmentTypes.Remove(equipmentType);
            _uow.SaveChanges();
            return true;
        }
    }
}

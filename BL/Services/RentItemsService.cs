﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.IServices;
using BL.IFactories;
using BL.DTO;
using DAL;

namespace BL.Services
{
    public class RentItemsService : IRentItemsService
    {
        private readonly IUnitOfWork _uow;
        private readonly IRentItemFactory _rentItemFactory;
        public RentItemsService(IUnitOfWork uow, IRentItemFactory rentItemFactory)
        {
            _uow = uow;
            _rentItemFactory = rentItemFactory;
        }

        public RentItemDTO Add(RentItemDTO dto)
        {
            var rentItem = _rentItemFactory.Transform(dto);
            _uow.RentItems.Add(rentItem);
            _uow.SaveChanges();
            return _rentItemFactory.Transform(rentItem);

        }

        public List<RentItemDTO> GetAll()
        {
            return _uow.RentItems.All().Where(p => p.IsVisible == true).Select(e => _rentItemFactory.Transform(e)).ToList();
        }

        public RentItemDTO GetById(long id)
        {
            var rentItem = _uow.RentItems.Find(id);
            if (rentItem == null || rentItem.IsVisible == false)
            {
                return null;
            }
            return _rentItemFactory.Transform(rentItem);
        }

        public List<RentItemDTO> GetAllByEquipmentId(long id)
        {
            return _uow.RentItems.Get(filter: p => p.EquipmentId == id).Select(p => _rentItemFactory.Transform(p)).ToList();
        }

        public RentItemDTO Put(RentItemDTO dto)
        {
            var rentItem = _uow.RentItems.Find(dto.RentItemId);
            if (rentItem == null)
            {
                return null;
            }
            _uow.RentItems.Detach(rentItem);
            rentItem = _rentItemFactory.Transform(dto);
            _uow.RentItems.Update(rentItem);
            _uow.SaveChanges();
            return _rentItemFactory.Transform(rentItem);
        }

        public bool Delete(long id)
        {
            var rentItem = _uow.RentItems.Find(id);
            if (rentItem == null)
            {
                return false;
            }
            _uow.RentItems.Remove(rentItem);
            _uow.SaveChanges();
            return true;
        }

        public List<RentItemDTO> GetAllByRentId(long id)
        {
            return _uow.RentItems.GetAllByRentId(id).Select(p => _rentItemFactory.Transform(p)).ToList();
        }
    }
}

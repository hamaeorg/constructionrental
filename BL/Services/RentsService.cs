﻿using BL.DTO;
using BL.IFactories;
using BL.IServices;
using DAL;
using Domain.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
    public class RentsService : IRentsService
    {
        private readonly IUnitOfWork _uow;
        private readonly IRentFactory _rentFactory;

        public RentsService(IUnitOfWork uow, IRentFactory rentFactory)
        {
            _uow = uow;
            _rentFactory = rentFactory;
        }
        public RentDTO Add(RentDTO dto)
        {
            var rent = _rentFactory.Transform(dto);
            _uow.Rents.Add(rent);
            _uow.SaveChanges();
            return _rentFactory.Transform(rent);
        }
        
        public RentDTO GetOrCreateRent(RentDTO dto)
        {
            var rent = GetByStatus("Open").FirstOrDefault();
            if (rent == null || rent.IsVisible == false)
            {;
                return Add(dto);
            }
            return rent;
        }

        public List<RentDTO> GetByStatus(string status)
        {
            var rents = _uow.Rents
                .FindByStatus((Status)Enum.Parse(typeof(Status), status))
                .Select(e => _rentFactory.TransformWithRentItems(e))
                .Where(p => p.IsVisible == true)
                .ToList();
            if (rents == null)
            {
                return null;
            }
            return rents;
        }

        public List<RentDTO> GetAll()
        {
            return _uow.Rents.All().Where(p => p.IsVisible == true).Select(e => _rentFactory.TransformWithRentItems(e)).ToList();
        }

        public RentDTO GetById(long id)
        {
            var rent = _uow.Rents.Find(id);
            if (rent == null || rent.IsVisible == false)
            {
                return null;
            }
            return _rentFactory.TransformWithRentItems(rent);
        }
        public RentDTO Put(RentDTO dto)
        {
            var rent = _uow.Rents.Find(dto.RentId);
            if (rent == null)
            {
                return null;
            }
            _uow.Rents.Detach(rent);
            rent = _rentFactory.Transform(dto);
            _uow.Rents.Update(rent);
            _uow.SaveChanges();
            return _rentFactory.Transform(rent);
        }
        public bool Delete(long id)
        {
            var rent = _uow.Rents.Find(id);
            if (rent == null)
            {
                return false;
            }
            _uow.Rents.Remove(rent);
            _uow.SaveChanges();
            return true;
        }    
    }
}

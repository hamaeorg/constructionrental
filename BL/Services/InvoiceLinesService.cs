﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.IServices;
using BL.DTO;

namespace BL.Services
{
    public class InvoiceLinesService : IInvoiceLinesService
    {
        private readonly IEquipmentTypePricesService _equipmentTypePricesService;
        public InvoiceLinesService(IEquipmentTypePricesService equipmentTypePricesService)
        {
            _equipmentTypePricesService = equipmentTypePricesService;
        }

        public List<InvoiceLineDTO> GenerateInvoiceLinesFromRent(RentDTO rentDTO)
        {
            return rentDTO.RentItems.ConvertAll(x => GenerateInvoiceLineFromRentItem(x));
        }

        public InvoiceLineDTO GenerateInvoiceLineFromRentItem(RentItemDTO rentItemDTO)
        {
            var eqTypePricesforThisInvoiceLine = _equipmentTypePricesService.GetByEquipmentTypeId(rentItemDTO.Equipment.EquipmentTypeId);

            var invoiceLine = new InvoiceLineDTO {};

            invoiceLine.RentItemId = rentItemDTO.RentItemId;
            invoiceLine.RentItem = rentItemDTO;
            invoiceLine.EquipmentTypePrices = eqTypePricesforThisInvoiceLine;
            invoiceLine.LoyaltyPoints = rentItemDTO.Equipment.EquipmentType.LoyaltyPoints;
            invoiceLine.Sum = calculateInvoiceLineSum(eqTypePricesforThisInvoiceLine, rentItemDTO.Days);

            return invoiceLine;
        }

        public decimal calculateInvoiceLineSum(List<EquipmentTypePriceDTO> eqTypePrices, int days)
        {
            var sum = new decimal(0);
            var daysLeft = days;
            foreach (EquipmentTypePriceDTO eqTypePrice in eqTypePrices)
            {
                var eqTypePriceSum = new decimal(0);
                var eqTypePriceDuration = eqTypePrice.Duration;

                if (eqTypePrice.Price.IsRecurrent)
                {
                    var rentDuration = (daysLeft >= eqTypePriceDuration) ? eqTypePriceDuration : daysLeft;
                    eqTypePriceSum = calculateRecurringRent(daysLeft, eqTypePriceDuration, eqTypePrice.Price.PriceValue, rentDuration);
                    daysLeft -= rentDuration;
                }
                else
                {
                    eqTypePriceSum = eqTypePrice.Price.PriceValue;
                }
                sum += eqTypePriceSum;
            }
            return sum;
        }

        private static decimal calculateRecurringRent(int daysLeft, int eqTypePriceDuration, decimal priceValue, int rentDuration)
        {
            if (eqTypePriceDuration != 0)
            {
                return priceValue * rentDuration;
            }
            else
            {
                return priceValue * daysLeft;
            }
        }
    }
}

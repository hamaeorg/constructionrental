﻿using BL.DTO;
using BL.IFactories;
using BL.IServices;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.Services
{
    public class InvoiceService : IInvoiceService
    {
        private readonly IInvoiceLinesService _invoiceLinesService;
        private readonly IRentsService _rentsService;
        private readonly IInvoiceFactory _invoiceFactory;

        public InvoiceService(IInvoiceLinesService invoiceLinesService, IRentsService rentsService, IInvoiceFactory invoiceFactory)
        {
            _invoiceLinesService = invoiceLinesService;
            _rentsService = rentsService;
            _invoiceFactory = invoiceFactory;
        }

        public InvoiceDTO GenerateInvoiceByRentId(long id)
        {
            var rentDTO = _rentsService.GetById(id);
            var invoiceLines = _invoiceLinesService.GenerateInvoiceLinesFromRent(rentDTO);
            return CalculateInvoice(_invoiceFactory.GetInvoiceBase(), invoiceLines);
        }

        public InvoiceDTO CalculateInvoice(InvoiceDTO invoice, List<InvoiceLineDTO> invoiceLines)
        {
            invoice.Price = invoiceLines.Sum(e => e.Sum);
            invoice.PriceWithVAT = PercentCalculator(invoice.VATrate, invoice.Price);
            invoice.InvoiceLines = invoiceLines;
            invoice.LoyaltyPoints = invoiceLines.Sum(e => e.LoyaltyPoints);
            return invoice;
        }

        public decimal PercentCalculator(decimal VATrate, decimal price)
        {
            return (decimal)(((VATrate / 100) + 1) * price);
        }
    }
}

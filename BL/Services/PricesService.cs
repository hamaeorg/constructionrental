﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.IServices;
using BL.IFactories;
using BL.DTO;
using DAL;

namespace BL.Services
{
    public class PricesService : IPricesService
    {
        private readonly IUnitOfWork _uow;
        private readonly IEquipmentTypePricesService _equipmentTypePricesService;
        private readonly IPriceFactory _priceFactory;
        public PricesService(IUnitOfWork uow, IEquipmentTypePricesService equipmentTypePricesService, IPriceFactory priceFactory)
        {
            _uow = uow;
            _equipmentTypePricesService = equipmentTypePricesService;
            _priceFactory = priceFactory;
        }

        public PriceDTO Add(PriceDTO dto)
        {
            var price = _priceFactory.Transform(dto);
            _uow.Prices.Add(price);
            _uow.SaveChanges();
            return _priceFactory.Transform(price);
        }
        public List<PriceDTO> GetAll()
        {
            return _uow.Prices.All().Where(p => p.IsVisible == true).Select(e => _priceFactory.Transform(e)).ToList();
        }

        public PriceDTO GetById(long id)
        {
            var price = _uow.Prices.Find(id);
            if (price == null || price.IsVisible == false)
            {
                return null;
            }
            return _priceFactory.Transform(price);
        }
        public PriceDTO MarkHidden(long id)
        {
            var price = _uow.Prices.Find(id);
            if (price == null)
            {
                return null;
            }
            _uow.Prices.Detach(price);
            price.IsVisible = false;
            _uow.Prices.Update(price);
            _uow.SaveChanges();
            return _priceFactory.Transform(price);
        }
        public bool Delete(long id)
        {
            var price = _uow.Prices.Find(id);
            if (price == null)
            {
                return false;
            }
            _uow.Prices.Remove(price);
            _uow.SaveChanges();
            return true;
        }
    }
}

﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.IServices;
using BL.IFactories;
using BL.DTO;
using DAL;

namespace BL.Services
{
    public class EquipmentService : IEquipmentService
    {
        private readonly IUnitOfWork _uow;
        private readonly IEquipmentFactory _equipmentFactory;
        
        public EquipmentService(IUnitOfWork uow,  IEquipmentFactory equipmentFactory)
        { 
            _uow = uow;
            _equipmentFactory = equipmentFactory;
        }

        public EquipmentDTO Add(EquipmentDTO dto)
        {
            var equipment = _equipmentFactory.Transform(dto);
            _uow.Equipment.Add(equipment);
            _uow.SaveChanges();
            return _equipmentFactory.Transform(equipment);
        }
        public List<EquipmentDTO> GetAll()
        {
            return _uow.Equipment.All().Where(p => p.IsVisible == true).Select(e => _equipmentFactory.Transform(e)).ToList();
        }

        public EquipmentDTO GetById(long id)
        {
            var equipment = _uow.Equipment.Find(id);
            if (equipment == null || equipment.IsVisible == false)
            {
                return null;
            }
            return _equipmentFactory.Transform(equipment);
        }

        public EquipmentDTO MarkHidden(long id)
        {
            var equipment = _uow.Equipment.Find(id);
            if (equipment == null)
            {
                return null;
            }
            _uow.Equipment.Detach(equipment);
            equipment.IsVisible = false;
            _uow.Equipment.Update(equipment);
            _uow.SaveChanges();
            return _equipmentFactory.Transform(equipment);
        }

        public bool Delete(long id)
        {
            var equipment = _uow.Equipment.Find(id);
            if (equipment == null)
            {
                return false;
            }
            _uow.Equipment.Remove(equipment);
            _uow.SaveChanges();
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.IServices;
using BL.IFactories;
using DAL.IEF;
using BL.DTO;
using DAL;

namespace BL.Services
{
    public class EquipmentTypePricesService : IEquipmentTypePricesService
    {
        private readonly IUnitOfWork _uow;
        private readonly IEquipmentTypePriceFactory _equipmentTypePriceFactory;
        public EquipmentTypePricesService(IUnitOfWork uow, IEquipmentTypePriceFactory equipmentTypePriceFactory)
        {
            _uow = uow;
            _equipmentTypePriceFactory = equipmentTypePriceFactory;
        }

        public EquipmentTypePriceDTO Add(EquipmentTypePriceDTO dto)
        {
            var equipment = _equipmentTypePriceFactory.Transform(dto);
            _uow.EquipmentTypePrices.Add(equipment);
            _uow.SaveChanges();
            return _equipmentTypePriceFactory.Transform(equipment);
        }
        public List<EquipmentTypePriceDTO> GetAll()
        {
            return _uow.EquipmentTypePrices.All().Where(p => p.IsVisible == true).Select(e => _equipmentTypePriceFactory.Transform(e)).ToList();
        }

        public EquipmentTypePriceDTO GetById(long id)
        {
            var equipmentTypePrice = _uow.EquipmentTypePrices.Find(id);
            if (equipmentTypePrice == null || equipmentTypePrice.IsVisible == false)
            {
                return null;
            }
            return _equipmentTypePriceFactory.Transform(equipmentTypePrice);
        }

        public List<EquipmentTypePriceDTO> GetByEquipmentTypeId(long id)
        {
            return _uow.EquipmentTypePrices
                .FindByEquipmentTypeId(id)
                .Select(p => _equipmentTypePriceFactory.Transform(p))
                .Where(p => p.IsVisible == true)
                .ToList();
        }

        public EquipmentTypePriceDTO MarkHidden(long id)
        {
            var equipmentTypePrice = _uow.EquipmentTypePrices.Find(id);
            if (equipmentTypePrice == null)
            {
                return null;
            }
            _uow.EquipmentTypePrices.Detach(equipmentTypePrice);
            equipmentTypePrice.IsVisible = false;
            _uow.EquipmentTypePrices.Update(equipmentTypePrice);
            _uow.SaveChanges();
            return _equipmentTypePriceFactory.Transform(equipmentTypePrice);
        }

        public bool Delete(long id)
        {
            var equipmentTypePrice = _uow.EquipmentTypePrices.Find(id);
            if (equipmentTypePrice == null)
            {
                return false;
            }
            _uow.EquipmentTypePrices.Remove(equipmentTypePrice);
            _uow.SaveChanges();
            return true;
        }
    }
}
